import axios from 'axios';

const URL = 'localhost:5000'


export let getSimpleServerResponse = () => {
    axios.get(`http://${URL}/simpleServerResponse`).then(res => {
        console.log(res);
        window.alert(res.data)
        
    }).catch(err => {
        console.log(err);
    })
}

export let getSimpleDBResponse = () => {
    axios.get(`http://${URL}/simpleDBResponse`).then(res => {

     console.log(res);
        window.alert(res.data)
        
    }).catch(err => {
        console.log(err);
    })
}
