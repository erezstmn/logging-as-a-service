import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import { getSimpleServerResponse, getSimpleDBResponse } from '../services/http';


export default () => {
    let onServerClickHandler = () => {
        getSimpleServerResponse();
    }

    let onDBClickHandler = () => {
        getSimpleDBResponse();
    }

    return (
        <div>
            <h1>Home component</h1>
            <ButtonToolbar>
                <Button onClick={onServerClickHandler}>Get response from server</Button>
                <Button onClick={onDBClickHandler}>Get response from db</Button>
               
      
            </ButtonToolbar>
        </div>
    )
}