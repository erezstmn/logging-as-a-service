import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './Home';
import GettingStarted from './GettingStarted';
import Dashboard from './Dashboard';


export default () => {
    return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/gettingStarted" component={GettingStarted}/>
            <Route exact path="/dashboard" component={Dashboard}></Route>
        </Switch>
    )
}

