import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavItem from 'react-bootstrap/NavItem';
import { Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';

import RoutsSwitch from './RoutesSwitch';



export default () => {
    return (
        <><div>
            <Navbar bg="dark">
                <Navbar.Brand>
                    <Link to="/">{'Logging-As-A-Service'}</Link>
                </Navbar.Brand>
                <Nav>

                    <LinkContainer to="/gettingStarted">
                        <Nav.Link>Getting Started</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/dashboard" >
                        <Nav.Link>Dashboard</Nav.Link>
                    </LinkContainer>
                </Nav>
            </Navbar>
        </div>
            <div>
                <RoutsSwitch></RoutsSwitch>
            </div>
        </>
    )
}