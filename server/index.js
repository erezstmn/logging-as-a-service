const express = require('express');
const app = express();
const db = require('./db/db')

const PORT = process.env.PORT || 5000;
const PRIVATE_IP = process.env.PRIVATE_IP || '127.0.0.1'

app.use((req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    next()

})

app.get('/simpleServerResponse',(req,res) => {
    res.send(`This is a message from the server, my IP is ${PRIVATE_IP}`)
  
})
app.get('/simpleDBResponse',(req,res) => {
    let message  = db.startSession().then(Response => {
        res.send(Response)
    });
    
    
})



app.listen(PORT, () => {
    console.log(`app is listening on port: ${PORT}`);    
})