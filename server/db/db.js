const MYSQL = require('mysql');
const CREDENTIALS = require('./credentials')



let startSession =  () => (new Promise((resolve, reject) => {
    let connection = MYSQL.createConnection({
        host: CREDENTIALS.HOST,
        user: CREDENTIALS.USER,
        password: CREDENTIALS.PASSWORD,
        database: CREDENTIALS.DATABASE,
    })
    connection.connect((err) => {
        if (err) {
            console.log(err);
            reject(err)   
        }
        connection.end((err) => {

        });
        resolve(`connected as id ${connection.threadId} `);
       

   })
}));
module.exports = {
    startSession: startSession
}

